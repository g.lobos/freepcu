<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use JWTAuth;
use Validator;
use Carbon\Carbon;

use Unlu\Laravel\Api\QueryBuilder;
use Dingo\Api\Routing\Helpers;
use App\Post;

class PostController extends Controller
{
	use Helpers;

    public function getAllPosts(Request $request){
    	try{
            $queryBuilder = new QueryBuilder(new Post, $request);
            $data = $queryBuilder->build()->paginate();
        }
        catch(Unlu\Laravel\Api\Exceptions\UnknownColumnException $e){
           return response()->json($e);
        }
        catch(Unlu\Laravel\Api\Exceptions\EmptyColumnsException $e){
           return response()->json($e);
        }

        return response()->json($data);
    }

    public function getPost($idPost){
    	return Post::findOrFail($idPost);
    }

    public function storePost(Request $request){
        try{
            $validator = Validator::make($request->all(),[
                'title' => 'required|min:1|max:240',
                'subtitle' => 'required|min:1|max:240',
                'content' => 'required|min:1',
                'img_path' => 'required|min:1',
            ]);

            if($validator->fails()){
                return response()->json($validator->messages(),400);
            }
            $post = new Post;
            $post->title = $request->title;
            $post->subtitle = $request->subtitle;
            $post->content = $request->content;
            $post->img_path = $request->img_path;
            $post->published_date = Carbon::now();;
            $post->status = 0;
            $post->user_id = Auth::user()->id;
            $post->save();
        }
        catch(Exception $e){
            return response()->json(['error' => 'Error when creating resource'], 400);
        }

        return response()->json(['success' => 'Post created successfully'], 201);
    }

    public function updatePost(Request $request, $idPost){
        try{
            $validator = Validator::make($request->all(),[
                'title' => 'required|min:1|max:240',
                'subtitle' => 'required|min:1|max:240',
                'content' => 'required|min:1',
                'img_path' => 'required|min:1',
                'status' => 'required',
            ]);
            if($validator->fails()){
                return response()->json($validator->messages(),400);
            }
            $post = Post::findOrFail($idPost);
            $post->title = $request->title;
            $post->subtitle = $request->subtitle;
            $post->content = $request->content;
            $post->img_path = $request->img_path;
            $post->published_date = Carbon::now();;
            $post->status = $request->status;
            $post->user_id = Auth::user()->id;
            $post->save();

        }
        catch(Exception $e){
            return response()->json(['error' => 'Error when creating resource'], 400);
        }

        return response()->json(['success' => 'Post updated successfully'], 201);
    }

    public function deletePost($idPost){
        if(Post::destroy($idPost)){
            return response()->json(['success' => 'Post destroyed successfully'], 201);
        }else{
            return response()->json(['error' => 'Post doesnt exist'], 404);
        }
    }
}
