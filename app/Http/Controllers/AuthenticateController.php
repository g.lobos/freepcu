<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Tymon\JWTAuth\Exception\JWTException;
use Validator;
use Tymon\JWTAuth\JWTAuth;

class AuthenticateController extends Controller
{
    protected $auth;

    public function __construct(JWTAuth $auth){
        $this->auth = $auth;
    }


    public function authenticateUser(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|min:1',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(),400);
        }
        
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = $this->auth->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
            /* All good, response with token */
        return response()->json(compact('token'));
    }
    

    public function createUser(Request $request){

        $validator = Validator::make($request->all(),[
            'email' => 'required|unique:users|min:1',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(),400);
        }
        /* Capture credentials */
        $credentials = $request->only('email', 'password');

        try{
             /* Trying to find a user with same email */
            $newUser = User::where('email', $request->email)->first();

            if($newUser == null){
                $credentials['password'] = bcrypt($credentials['password']);
                $newUser = User::create($credentials);
            }else{
                return response()->json(['error' => 'User already exists'], 409);
            }

        }catch(Exception $e){
            return response()->json(['error' => 'Error when creating user'], 409);
        }
        $token = $this->auth->fromUser($newUser);
        return response()->json(compact('token'));
    }
}
