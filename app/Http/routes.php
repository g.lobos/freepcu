<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

$api = app('Dingo\Api\Routing\Router');

/* API Routes without Auth */
$api->version('v1', ['middleware' => 'api.throttle', 'limit' => 100, 'expires' => 5], function ($api) {
	/* AUTH */
    $api->post('/authenticate', 'App\Http\Controllers\AuthenticateController@authenticateUser');
    $api->post('/signup', 'App\Http\Controllers\AuthenticateController@createUser');

    /* Posts */
    $api->get('/posts', 'App\Http\Controllers\PostController@getAllPosts');
    $api->get('/posts/{idPost}', 'App\Http\Controllers\PostController@getPost');
});

/* JWT Protected Routes */
$api->version('v1', ['middleware' => ['cors','api.auth','jwt.auth'], 'providers' => 'jwt'], function ($api) {
	$api->get('/getauth', 'App\Http\Controllers\PostController@getAuthenticatedUser');
    $api->post('/posts', 'App\Http\Controllers\PostController@storePost');
    $api->post('/posts/{idPost}', 'App\Http\Controllers\PostController@updatePost');
    $api->delete('/posts/{idPost}', 'App\Http\Controllers\PostController@deletePost');
});